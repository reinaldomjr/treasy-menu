import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Injectable } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

/**
 * Node for to-do item
 */
export class MenuNode {
  id: string;
  name: string;
  children: MenuNode[];
}

/** Flat to-do item node with expandable and level information */
export class MenuFlatNode {
  name: string;
  id: string;
  level: number;
  expandable: boolean;
}

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<MenuNode[]>([]);

  get data(): MenuNode[] { return this.dataChange.value; }

  constructor(private http: HttpClient) {
    this.initialize();
  }

  initialize() {
    // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
    //     file node as children.

    this.http.get('http://localhost:8080/menu/')
      .subscribe((result) => {
        const data = this.buildFileTree(result['children'], 0);
        // Notify the change.
        this.dataChange.next(data);
      }, (error) => {
        const data = this.buildFileTree([], 0);
        // Notify the change.
        this.dataChange.next(data);
      });
  }

  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `TodoItemNode`.
   */
  buildFileTree(children: [], level: number): MenuNode[] {
    if(!children || children.length <= 0) {
      const node = new MenuNode();
      node.name = '';
      return [node];
    }

    return children.reduce<MenuNode[]>((accumulator, menu: object) => {
      const node = new MenuNode();
      node.name = menu['data']['name'];
      node.id = menu['data']['@rid'];
      if (menu['children'] && menu['children']['length'] > 0) {
        node.children = this.buildFileTree(menu['children'], level + 1);
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Add an item to to-do list */
  insertItem(parent: MenuNode, name: string) {
    if (!parent.children) {
      parent.children = [];
    }
    parent.children.push({ name: name } as MenuNode);
    this.dataChange.next(this.data);
  }

  updateItem(node: MenuNode, name: string) {
    node.name = name;
    node.id = '#13:10';
    this.dataChange.next(this.data);
  }
}

/**
 * @title Tree with checkboxes
 */
@Component({
  selector: 'app-tree-menu',
  templateUrl: 'tree-menu.component.html',
  styleUrls: ['tree-menu.component.css'],
  providers: [ChecklistDatabase]
})
export class TreeMenuComponent {
  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<MenuFlatNode, MenuNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<MenuNode, MenuFlatNode>();

  /** A selected parent node to be inserted */
  selectedParent: MenuFlatNode | null = null;

  /** The new item's name */
  newItemName = '';

  treeControl: FlatTreeControl<MenuFlatNode>;

  treeFlattener: MatTreeFlattener<MenuNode, MenuFlatNode>;

  dataSource: MatTreeFlatDataSource<MenuNode, MenuFlatNode>;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<MenuFlatNode>(true /* multiple */);

  constructor(private _database: ChecklistDatabase, private http: HttpClient) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<MenuFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    _database.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
  }

  getLevel = (node: MenuFlatNode) => node.level;

  isExpandable = (node: MenuFlatNode) => node.expandable;

  getChildren = (node: MenuNode): MenuNode[] => node.children;

  hasChild = (_: number, _nodeData: MenuFlatNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: MenuFlatNode) => _nodeData.name === '';

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: MenuNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.name === node.name
      ? existingNode
      : new MenuFlatNode();
    flatNode.name = node.name;
    flatNode.id = node.id;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: MenuFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: MenuFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: MenuFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: MenuFlatNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: MenuFlatNode): void {
    let parent: MenuFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: MenuFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  /* Get the parent node of a node */
  getParentNode(node: MenuFlatNode): MenuFlatNode | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  /** Select the category so we can insert the new item. */
  addNewItem(node: MenuFlatNode) {
    const parentNode = this.flatNodeMap.get(node);
    this._database.insertItem(parentNode, '');
    this.treeControl.expand(node);
  }

  /** Save the node to database */
  saveNode(node: MenuFlatNode, itemValue: string) {
    const parentNode = this.getParentNode(node);
    if (parentNode) {
      console.log(parentNode.id);
      const menu = {
        "name": itemValue
      };

      var url = 'http://localhost:8080/menu/' + encodeURIComponent(parentNode.id) + '/add-submenu';

      console.log(url);

      this.http
        .post(url, menu)
        .subscribe((result) => {
          console.log(result);
        });
    } else {
      const menu = {
        "name": itemValue
      };
      var url = 'http://localhost:8080/menu/';
      console.log(url);
      this.http
        .post(url, menu)
        .subscribe((result) => {
          console.log(result);
        });
    }
    const nestedNode = this.flatNodeMap.get(node);
    this._database.updateItem(nestedNode, itemValue);
  }
}


/**  Copyright 2019 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */