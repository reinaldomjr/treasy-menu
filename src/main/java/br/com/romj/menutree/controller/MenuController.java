package br.com.romj.menutree.controller;

import br.com.romj.menutree.service.MenuService;
import br.com.romj.menutree.service.dto.CreateMenuDTO;
import br.com.romj.menutree.service.dto.MenuDTO;
import br.com.romj.menutree.service.dto.NodeTreeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuService service;

    @GetMapping("/")
    public ResponseEntity<NodeTreeDTO<MenuDTO>> getRootMenu(
            @RequestParam(name = "nameFilter", required = false) String nameFilter) {
        return ResponseEntity.ok(service.getMenu(null, nameFilter));
    }

    @GetMapping("/{menuId}")
    public ResponseEntity<NodeTreeDTO<MenuDTO>> getMenu(
            @PathVariable(name = "menuId") String menuId,
            @RequestParam(name = "nameFilter", required = false) String nameFilter) {
        return ResponseEntity.ok(service.getMenu(menuId, nameFilter));
    }

    @PutMapping("/{menuId}")
    public ResponseEntity<MenuDTO> updateMenu(
            @PathVariable(name = "menuId") String menuId,
            @RequestBody CreateMenuDTO menu) {
        return ResponseEntity.ok(service.updateMenu(menuId, menu));
    }

    @DeleteMapping("/{menuId}")
    public ResponseEntity<NodeTreeDTO<MenuDTO>> deleteMenu(
            @PathVariable(name = "menuId") String menuId) {
        return ResponseEntity.ok(service.deleteMenu(menuId));
    }

    @PostMapping("/")
    public ResponseEntity<MenuDTO> addRootMenu(
            @RequestBody CreateMenuDTO menu) {
        return ResponseEntity.ok(service.addMenu(menu, null));
    }

    @PostMapping("/{menuId}/add-submenu")
    public ResponseEntity<MenuDTO> addSubmenu(
            @PathVariable(name = "menuId") String menuId,
            @RequestBody CreateMenuDTO menu) {
        return ResponseEntity.ok(service.addMenu(menu, menuId));
    }
}
