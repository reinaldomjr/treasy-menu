package br.com.romj.menutree.service;

import br.com.romj.menutree.domain.JsonConverter;
import br.com.romj.menutree.domain.Menu;
import br.com.romj.menutree.exceptions.ConflictException;
import br.com.romj.menutree.persistence.MenuDAO;
import br.com.romj.menutree.service.dto.CreateMenuDTO;
import br.com.romj.menutree.service.dto.MenuDTO;
import br.com.romj.menutree.service.dto.NodeTreeDTO;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class MenuService {

    @Autowired
    private MenuDAO dao = null;

    public MenuDTO addMenu(CreateMenuDTO menuDTO, String parentMenu) {
        Menu menu = new Menu(menuDTO.getName());
        try {
            return JsonConverter.convert(dao.insert(menu, parentMenu), MenuDTO.class);
        } catch (IOException e) {
            throw new InternalError("Error parsing response.", e);
        } catch (ORecordDuplicatedException duplicated) {
            throw new ConflictException();
        }
    }

    public NodeTreeDTO<MenuDTO> getMenu(String menuId, String nameFilter) {

        Map<Menu, Set<Menu>> menuMap = dao.getMenu(menuId, nameFilter);
        Optional<Menu> rootNode = Optional.empty();
        if (StringUtils.isNotBlank(menuId)) {
            rootNode = menuMap.keySet().stream().filter(menu -> menu != null && menu.getId().equals(menuId)).findAny();
        }
        return createTree(rootNode.orElse(null), menuMap);

    }

    private <T> NodeTreeDTO<MenuDTO> createTree(T nodeData, Map<T, Set<T>> nodesMap) {
        try {
            NodeTreeDTO<MenuDTO> node = new NodeTreeDTO<>(JsonConverter.convert(nodeData, MenuDTO.class));
            nodesMap.getOrDefault(nodeData, new HashSet<>())
                    .forEach(s -> node.add(createTree(s, nodesMap)));
            return node;
        } catch (IOException e) {
            throw new InternalError("Error parsing response.", e);
        }

    }

    public NodeTreeDTO<MenuDTO> deleteMenu(String menuId) {
        return createTree(null, dao.delete(menuId));
    }

    public MenuDTO updateMenu(String menuId, CreateMenuDTO menuDTO) {
        try {
            Menu menu = JsonConverter.convert(menuDTO, Menu.class);
            menu.setId(menuId);
            return JsonConverter.convert(dao.update(menu), MenuDTO.class);
        } catch (IOException e) {
            throw new InternalError("Error parsing response.", e);
        }
    }
}
