package br.com.romj.menutree.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class NodeTreeDTO<T> {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private T data;
    private final Set<NodeTreeDTO> children = new HashSet<>();

    public NodeTreeDTO(T data) {
        this.data = data;
    }

    public Set<NodeTreeDTO> getChildren() {
        return children;
    }

    public int size() {
        return children.size() + children.stream().map(NodeTreeDTO::size).reduce(0, Integer::sum);
    }

    public boolean contains(Object o) {
        return children.contains(o) || children.stream().anyMatch(menuNode -> menuNode.contains(o));
    }

    public boolean add(NodeTreeDTO menuNode) {
        return children.add(menuNode);
    }

    public boolean remove(Object o) {
        return children.remove(o);
    }

    public boolean addAll(Collection<? extends NodeTreeDTO> collection) {
        return children.addAll(collection);
    }

    public boolean removeAll(Collection<?> collection) {
        return children.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        return children.retainAll(collection);
    }

    public void clear() {
        children.forEach(NodeTreeDTO::clear);
        children.clear();
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
