package br.com.romj.menutree.service.dto;

public class CreateMenuDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
