package br.com.romj.menutree.config;

import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.db.ODatabaseType;
import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.OrientDBConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseConfig {
    @Value("${orientdb.host}")
    private String host;
    @Value("${orientdb.database.name}")
    private String databaseName;
    @Value("${orientdb.database.userName}")
    private String dbUserName;
    @Value("${orientdb.database.userPassword}")
    private String dbUserPassword;
    @Value("${orientdb.root-password}")
    private String rootPassword;

    @Autowired
    private OrientDB db;

    @Bean
    public OrientDB configure() {

        OrientDB orientDB = new OrientDB("remote:" + host, "root", rootPassword, OrientDBConfig.defaultConfig());
        return orientDB;
    }

    @Bean
    public ODatabaseSession databaseSession () {
        db.createIfNotExists(databaseName, ODatabaseType.PLOCAL);
        return db.open(databaseName, dbUserName, dbUserPassword);
    }

}
