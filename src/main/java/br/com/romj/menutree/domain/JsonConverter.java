package br.com.romj.menutree.domain;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonConverter {

    private JsonConverter() {
    }

    public static <T> String toJson(T value) throws JsonProcessingException {
        return getMapper().writeValueAsString(value);
    }

    public static <T> T fromJson(String json, Class<T> clazz) throws IOException {
        return getMapper().readValue(json, clazz);
    }

    public static <T, O> T convert(O source, Class<T> clazz) throws IOException {
        return getMapper().readValue(toJson(source), clazz);
    }

    public static <O> O copy(O source) throws IOException {
        return convert(source, (Class<O>) source.getClass());
    }

    public static <T> T patchObject(T target, String patchJson) throws IOException {
        return getMapper().readerForUpdating(target).readValue(patchJson);
    }

    public static <T, O> T patchObject(T target, O source) throws IOException {
        String json = getMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(source);
        return patchObject(target, json);
    }

    private static ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }
}

