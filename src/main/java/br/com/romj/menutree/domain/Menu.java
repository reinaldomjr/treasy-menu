package br.com.romj.menutree.domain;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Menu implements Serializable {
    @JsonProperty("@rid")
    private String id;
    private String name;

    public Menu() {
    }

    public Menu(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Menu(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Menu menu = (Menu) o;
        return name.equals(menu.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
