package br.com.romj.menutree.persistence;

import br.com.romj.menutree.domain.JsonConverter;
import br.com.romj.menutree.domain.Menu;
import br.com.romj.menutree.exceptions.InvalidParamsException;
import br.com.romj.menutree.exceptions.NotFoundException;
import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class MenuDAO extends BaseDAO {
    private ODatabaseSession session;

    public MenuDAO(ODatabaseSession session) {
        session.activateOnCurrentThread();
        if (session.getClass(Menu.class.getSimpleName()) == null) {
            OClass vertexClass = session.createVertexClass(Menu.class.getSimpleName());
            vertexClass.createProperty("name", OType.STRING).setMandatory(true);
            session.command("CREATE INDEX uniqueIdx ON Menu (name COLLATE ci) UNIQUE");
        }
        if (session.getClass("childOf") == null) {
            session.createEdgeClass("childOf");
        }
        this.session = session;
    }

    public Menu insert(Menu menu, String childOf) throws IOException {
        session.activateOnCurrentThread();
        OVertex parent = null;
        if (StringUtils.isNotBlank(childOf)) {
            parent = session.getRecord(new ORecordId(childOf));
            if (parent == null) {
                throw new NotFoundException(childOf);
            }
        }
        OVertex record = session.newVertex(Menu.class.getSimpleName());
        record.setProperty("name", menu.getName());
        if (parent != null) {
            record.addEdge(parent, "childOf");
        }
        record.save();

        return JsonConverter.fromJson(record.toJSON(), Menu.class);
    }

    public Menu update(Menu menu) throws IOException {
        session.activateOnCurrentThread();
        OVertex record;
        if (menu == null || StringUtils.isBlank(menu.getId())) {
            throw new InvalidParamsException();
        }
        record = session.getRecord(new ORecordId(menu.getId()));
        if (record == null) {
            throw new NotFoundException(menu.getId());
        }
        record.setProperty("name", menu.getName());
        record.save();
        return JsonConverter.fromJson(record.toJSON(), Menu.class);
    }

    public Map<Menu, Set<Menu>> getMenu(String menuId, String nameFilter) {
        session.activateOnCurrentThread();
        Map<String, Object> params = new HashMap<>();

        String matchName = "";
        if (StringUtils.isNotBlank(nameFilter)) {
            matchName = " .in(childOf){class: Menu, while: (in(childOf).size() > 0), where: (name.toLowerCase() like :name AND out(childOf).size() > 0)}";
            params.put("name", "%" + nameFilter.toLowerCase() + "%");
        }

        String matchRoot = "{class: Menu, where: (out(childOf).size() = 0)}";
        if (menuId != null) {
            matchRoot = "{class: Menu, where: (@rid = :rootId)}";
            params.put("rootId", menuId);
        }

        String query = "select expand(@rid) from (" +
                " MATCH " + matchRoot +
                " .in(childOf){class: Menu, while: (in(childOf).size() > 0)}" +
                matchName +
                " RETURN distinct $pathElements" +
                ")";

        OResultSet queryResult = session.query(query, params);
        return processResult(queryResult, Menu.class);
    }

    public Map<Menu, Set<Menu>> delete(String menuId) {
        session.activateOnCurrentThread();

        OVertex parent = session.getRecord(new ORecordId(menuId));
        if (parent == null) {
            throw new NotFoundException(menuId);
        }

        Map<String, Object> params = new HashMap<>();
        params.put("rootId", menuId);

        String query = "delete vertex from(select expand(@rid) from (MATCH {class: Menu, where: (@rid = :rootId)}.in(childOf){class: Menu, while: (in(childOf).size() > 0)}RETURN distinct $pathElements))";

        OResultSet queryResult = session.command(query, params);
        return processResult(queryResult, Menu.class);
    }
}
