package br.com.romj.menutree.persistence;

import br.com.romj.menutree.domain.JsonConverter;
import br.com.romj.menutree.domain.Menu;
import com.orientechnologies.orient.core.record.ODirection;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BaseDAO {
    protected Menu vertexToEntity(OVertex vertex, Class<Menu> clazz) {
        try {
            return JsonConverter.fromJson(vertex.toJSON(), clazz);
        } catch (IOException e) {
            throw new InternalError("Error converting json to Entity", e);
        }
    }

    protected Map<Menu, Set<Menu>> processResult(OResultSet queryResult, Class<Menu> entityClass) {
        Map<Menu, Set<Menu>> map = new HashMap<>();
        try (OResultSet resultSet = queryResult) {
            resultSet.stream()
                    .filter(oResult -> oResult.isVertex() && oResult.getVertex().isPresent())
                    .map(oResult -> oResult.getVertex().get())
                    .forEach(vertex -> {
                        Menu node = vertexToEntity(vertex, entityClass);
                        Menu parent = null;
                        Iterable<OEdge> parents = vertex.getEdges(ODirection.OUT);
                        if (parents.iterator().hasNext()) {
                            OEdge childOf = parents.iterator().next();
                            parent = vertexToEntity(childOf.getTo(), entityClass);
                        }
                        if (map.get(parent) == null) {
                            map.put(parent, new HashSet<>());
                        }
                        Set<Menu> children = map.get(parent);
                        children.add(node);
                    });
        }
        return map;
    }
}
