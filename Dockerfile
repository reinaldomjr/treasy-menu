FROM openjdk:12.0.1
MAINTAINER Reinaldo machado <reinaldo.m.jr@gmail.com>

EXPOSE 8080

ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/service/service.jar"]

ARG JAR_FILE
ADD ${JAR_FILE} /usr/share/service/service.jar
