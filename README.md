-- Requisitos --

Maven 3.6
docker
docker-compose

-- Instruções --

Na raiz do projeto, executar o build do maven

*mvn clean install*

O build já gera os artefatos do Angular e do Docker

Após isso, executar na raíz do projeto o comando:

*docker-compose up*

Acesso ao sistema em: http://localhost:8080
Acesso à API em: http://localhost:8080/swagger-ui.html
